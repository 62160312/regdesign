import 'package:flutter/material.dart';

class Cost extends StatefulWidget {
  const Cost({Key? key}) : super(key: key);

  @override
  State<Cost> createState() => _CostState();
}

class _CostState extends State<Cost> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ภาระค่าใช้จ่ายทุน',style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: 60,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("ภาระค่าใช้จ่าย/ทุนการศึกษา",
                        style: TextStyle(color: Colors.orange, fontSize:30),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                //Height constraint at Container widget level
                child: new Image.asset('image/Cost1.png'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
