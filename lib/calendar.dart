import 'package:flutter/material.dart';

class calendar extends StatefulWidget {
  const calendar({Key? key}) : super(key: key);

  @override
  State<calendar> createState() => _calendarState();
}

class _calendarState extends State<calendar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ปฏิทินการศึกษา',style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: 60,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("ปฏิทินการศึกษา ...",
                        style: TextStyle(color: Colors.orange, fontSize:30),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                //Height constraint at Container widget level
                child: new Image.asset('image/Calendar1.png'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
