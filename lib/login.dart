
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:regdesign/Cost.dart';
import 'package:regdesign/ask.dart';
import 'package:regdesign/calendar.dart';
import 'package:regdesign/corse.dart';
import 'package:regdesign/profile.dart';
import 'package:regdesign/timetable.dart';

void main() {
  runApp(Login());
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 180,
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/th/thumb/e/ec/Buu-2.jpg/458px-Buu-2.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("ข่าวประชาสัมพันธ์",
                          style: TextStyle(color: Colors.black, fontSize:30),
                        ),
                      ),
                    ],
                  ),
                ),

                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 250,
                  child: Image.asset('image/pay652.jpg')
                ),
                Divider(
                  color: Colors.grey,
                ),

                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildProfileButton(context),
                      buildAskButton(context),
                      buildTableButton(context),

                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildMessageButton(context),
                      buildTableViewButton(context),
                      buildRecentButton(context),

                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildProfileButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.account_box_rounded ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => profile()),
          );
        },
        ),
      Text("บัญชีผู้ใช้งาน"),
    ],
  );
}


Widget buildAskButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.question_answer_outlined ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => ask()),
          );
        },
      ),
      Text("ตอบคำถาม"),
    ],
  );
}

Widget buildTableButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.list_alt_outlined ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => table()),
          );
        },
      ),
      Text("ตารางเรียน/สอบ"),
    ],
  );
}

Widget buildMessageButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Cost()),
          );
        },
      ),
      Text("ภาระค่าใช้จ่ายทุน"),
    ],
  );
}

Widget buildRecentButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.recent_actors ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) =>corse()),
          );
        },
      ),
      Text("หลักสูตรที่เปิดสอน"),
    ],
  );
}

Widget buildTableViewButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_month_outlined  ,
          color: Colors.amberAccent,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => calendar()),
          );
        },
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}

