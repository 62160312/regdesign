import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:regdesign/home.dart';
import 'login.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) =>
          MaterialApp(
            debugShowCheckedModeBanner: false,
            useInheritedMediaQuery: true,
            builder: DevicePreview.appBuilder,
            locale: DevicePreview.locale(context),
            title: 'Responsive and adaptive UI in Flutter',
            theme: ThemeData(
              primarySwatch: Colors.grey,
            ),
            home: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: const Text('มหาวิทยาลัยบูรพา'),
              ),
              drawer: Drawer(
                child: ListView(
                  children: [
                    UserAccountsDrawerHeader(
                      accountName: Text("62160312"),
                      accountEmail: Text("62160312@go.buu.ac.th"),
                      currentAccountPicture: CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://cdn.pixabay.com/photo/2015/09/09/14/02/icon-931551_960_720.jpg"),
                      ),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            "https://png.pngtree.com/thumb_back/fh260/back_our/20200701/ourmid/pngtree-cute-and-beautiful-hand-drawn-landscape-illustration-background-image_345580.jpg",
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                      otherAccountsPictures: [
                      ],
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) => home()),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.account_box),
                      title: Text("About"),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.grid_3x3_outlined),
                      title: Text("Products"),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.contact_mail),
                      title: Text("Contact"),
                      onTap: () {},
                    )
                  ],
                ),
              ),

              body: const Login(),
            ),
          ),
    );
  }
}
