import 'package:flutter/material.dart';

class corse extends StatefulWidget {
  const corse({Key? key}) : super(key: key);

  @override
  State<corse> createState() => _corseState();
}

class _corseState extends State<corse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('หลักสูตรที่เปิดสอน',style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: 60,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("หลักสูตร",
                        style: TextStyle(color: Colors.orange, fontSize:30),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                //Height constraint at Container widget level
                child: new Image.asset('image/cousre1.png'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
