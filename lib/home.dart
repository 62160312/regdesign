import 'package:flutter/material.dart';

class home extends StatefulWidget {
  const home({Key? key}) : super(key: key);

  @override
  State<home> createState() => _homeState();
}

class _homeState extends State<home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home',style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),),

      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                //Height constraint at Container widget level
                child: new Image.asset('image/buu1.jpg'),
              ),
              Container(
                width: double.infinity,
                //Height constraint at Container widget level
                child: new Image.asset('image/if.jpg'),
              ),
            ],
          ),
        ],
      ),
    );

  }
}
